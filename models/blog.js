const { unique } = require('faker');
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const schema = new Schema({
  title: {
    type: String,
    required: true,
    trim: true
  },
  subtitle: {
    type: String,
    required: true,
    trim: true
  },

  text: {
    type: String,
    required: true
  },
  
  img:
    {
        type:String, 
    },
    created: { 
        type: Date, 
        required: true, 
        default: Date.now }
  
});

module.exports = mongoose.model('Blogs', schema);