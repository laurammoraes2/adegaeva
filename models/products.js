const { unique } = require('faker');
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const schema = new Schema({
  name: {
    type: String,
    required: true,
    trim: true
  },
  description: {
    type: String,
    required: true
  },
  price:{
      type: Number, 
      required: true
  },
  quantity:{
      type: Number,
      required: true
  },
  
  img:
    {
        type:String, 
    },
    type: {
      type: String,
      required: true
    },
  
  
});

module.exports = mongoose.model('Products', schema);
