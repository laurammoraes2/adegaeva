const { unique } = require('faker');
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const schema = new Schema({
  name_customer: {
    type: String,
    required: true,
    trim: true
  },
  email_customer: {
      type: String,
      required: true,
  },
  tel_customer: {
      type: Number, 
      required: true,
  },
  cep_customer: { 
      type: String,
      required: true, 
  }, 
  address_customer:{
    type: String,
    required: true, 
}, 
   number_customer: {
    type: Number,
    required: true, 
   },
  cesta:{
    type: String,
    required: true, 
  },
  status:{
    type: String, 
    required: true,
  }
 
  
});

module.exports = mongoose.model('Requests', schema);
