const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const expressLayouts = require('express-ejs-layouts')
const faker = require('faker'); 
const path = require('path');
var http = require('http');
const passport = require('passport');
require('./config/auth')(passport);
const session = require('express-session');
const flash = require('connect-flash');
const bcrypt = require('bcryptjs');
const cookieParser = require('cookie-parser');
const mercadopago = require ('mercadopago');
var favicon = require('serve-favicon');




mercadopago.configure({
    access_token: 'APP_USR-8089417633582493-042900-b88dd22fe83bff1a9a81871762a71bde-463038908'
  });
  
  

// App
const app = express();
app.use(cookieParser())

app.use(session({
    secret: 'adegaeva',
    resave: true,
    saveUninitialized: true
}))
app.use(passport.initialize())
app.use(passport.session())


app.use(flash())

app.set('view engine', 'ejs');
app.set('views', path.join(__dirname, 'views'));
app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use(bodyParser.json());
app.use(expressLayouts);
app.use(express.static(__dirname + '/public'));
app.use(express.static('/public'));

app.use('/request', express.static(__dirname + '/public'));
app.use('/produtos', express.static(__dirname + '/public'));
app.use('/produtos/:id', express.static(__dirname + '/public'));
app.use('/admin/:id', express.static(__dirname + '/public'));
app.use('/blog/:id', express.static(__dirname + '/public'));
app.use('/shop/:type', express.static(__dirname + '/public'));
app.use('/novidades/:id', express.static(__dirname + '/public'));




// app.use(favicon(path.join(__dirname, 'public/images', 'favicon.ico')));
const middlewares = [
    // ...
    bodyParser.urlencoded({ extended: true }),
  ];

// Middleware
  app.use((req, res, next) => {
      res.locals.sucess_msg = req.flash("success_msg")
      res.locals.error_msg = req.flash("error_msg")
      res.locals.error = req.flash("error")
      res.locals.user = req.user || null;
      next()
  });
  


// Database



mongoose.connect('mongodb://localhost/api',  {
    useUnifiedTopology: true,
    useFindAndModify: true,
    useNewUrlParser: true,
    useCreateIndex: true 
}
);
const db = mongoose.connection;
  
db.on('connected', () => {
    console.log('Mongoose default connection is open');
});

db.on('error', err => {
    console.log(`Mongoose default connection has occured \n${err}`);
});

db.on('disconnected', () => {
    console.log('Mongoose default connection is disconnected');
   
});

process.on('SIGINT', () => {
    db.close(() => {
        console.log(
        'Mongoose default connection is disconnected due to application termination'
        );
        process.exit(0);
    });
});


// Load models
const Users = require('./models/users');
const Products = require('./models/products');
const Cestas = require('./models/cesta');
const Requests = require('./models/request');   
const Blogs = require('./models/blog');   




// Load routes
const indexRoutes = require('./routes/index-routes');
app.use('/', indexRoutes);



module.exports = app;