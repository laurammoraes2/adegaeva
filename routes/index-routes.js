const express = require('express');
const mongoose = require('mongoose');
const router = express.Router();
const usersController = require('../controllers/users');
const adminController = require('../controllers/admin');
const shopController = require('../controllers/shop');
const cartController = require('../controllers/carrinho');
const Products = mongoose.model('Products');
const Blog = mongoose.model('Blogs'); 

var cookieParser = require('cookie-parser');
const paymentController = require('../controllers/carrinho');
const { eAdmin } = require("../helpers/eAdmin");

const multer = require("multer");


var fs = require('fs');
var path = require('path');

const storage = multer.diskStorage({
  destination: function(request, file, callback){
    callback(null, './public/uploads'); 
  },
  filename: function(request, file, callback){
    callback(null,Date.now() + file.originalname)
  },
});

const upload = multer({
  storage:storage, 
  limits:{
    fieldSize: 1024*1024*3,

  }
});



 


router.get('/', adminController.home);
router.get('/shop', shopController.shop);
router.get('/shop/:type', shopController.shopFilter);


// Login e Logout
router.get('/login', (req,res) => {
  res.status(200).render('pages/login');
});
router.post('/login', usersController.listUser);

router.get('/logout', (req, res) => {
    req.logout()
    req.flash('success_msg', "Deslogado com sucesso")
    res.clearCookie('user');
    res.redirect('/')
}
)

//Submit of User
router.get('/submit',(req,res) => {
  res.status(200).render('pages/submit');
});
router.post('/submit', usersController.createUser);
// Carrinho e processo de compra

router.get('/cesta', cartController.listItems);
// router.post('/cesta', cartController.updatePrice);
router.get('/checkout', cartController.checkout);
router.post('/pagamento', cartController.finish);
router.post('/atualizar', cartController.addItem);

// Admin 
router.get('/users', eAdmin,  usersController.listUsers);
router.post('/create-user', eAdmin,  usersController.createUser);
router.put('/update-user/:id', eAdmin,   usersController.updateUser);
router.delete('/delete-user/:id', eAdmin,   usersController.deleteUser);

// Blog
router.get('/adminblog', eAdmin,  adminController.blog);
router.get('/create-blog', eAdmin,  adminController.createblog);
router.get('/blog/:id', eAdmin,  adminController.blogdetail);
router.get('/novidades',  adminController.novidades);
router.get('/novidades/:id',  adminController.novidadeArtigo);




router.post('/create-blog', eAdmin,  upload.single('filename'), async (req, res) => {
  try {
    console.log(req.body)
    const title = req.body.titulo;
    const text = req.body.texto;
    const img = req.file.filename;
    const subtitle = req.body.subtitulo;

 
  if (req.file == undefined) {
    return res.send(`You must select a file.`);
  }

  let blog = new Blog({
   title,
   subtitle,
   text,
    img,
    
   
   
  });
  blog.save().then(() => {
              
    res.redirect('/blog')
  }).catch((err) => {
    console.log(err)
  })
  

    
  } catch (error) {
    console.log(error)
  }
  
 
});
router.post('/updateblog/:id', eAdmin,   adminController.updateBlog);
router.get('/blog/:id/delete', eAdmin,   adminController.deleteBlog);

// Request - atualizações 
router.get('/request/:id/atualizarPagamento', eAdmin, cartController.confirmarPagamento);
router.get('/request/:id/falarCliente', eAdmin, cartController.falarCliente);
router.get('/request/:id/emseparacao', eAdmin, cartController.emSeparacao);
router.get('/request/:id/enviado', eAdmin, cartController.enviado);
router.get('/request/:id/finalizar', eAdmin, cartController.finalizar);






// Create Products
router.get('/dashboard', eAdmin, adminController.dashboard);
router.get('/produtos', eAdmin, adminController.products);
router.get('/create-product', eAdmin, adminController.createproducts);
// router.post('/create-product', eAdmin, adminController.formproducts);
router.post('/create-product', eAdmin,  upload.single('filename'), async (req, res) => {
    try {
      console.log(req.body)
      const name = req.body.name;
      const description = req.body.description;
      const price = req.body.price;
      const quantity = req.body.quantity;
      const img = req.file.filename;
      const type = req.body.tipo;

   console.log(img)
    if (req.file == undefined) {
      return res.send(`You must select a file.`);
    }

    let product = new Products({
      name,
      description, 
      price, 
      quantity, 
      img,
      type
      
     
     
    });
    product.save().then(() => {
                
      res.redirect('/produtos')
    }).catch((err) => {
      
    })
    

      
    } catch (error) {
      console.log(error)
    }
    
   
  });





// Pedidos 
router.get('/requests', eAdmin, adminController.requests)
router.get('/finalizados', eAdmin, adminController.pedidosFinalizados)


// Clientes 
router.get('/admin', eAdmin, adminController.customers)
router.get('/admin/:id', eAdmin, adminController.customersDetail)
router.post('/admin/:id/edit', eAdmin, adminController.editadmin)
router.get('/admin/:id/delete', eAdmin, adminController.deleteAdmin)



router.get('/create-admin', eAdmin, adminController.createAdmin)
router.post('/clientes', eAdmin, adminController.create)


// Produtos 
router.get('/:id', shopController.descriptionProd)
router.post('/:id', cartController.addItem )
router.get('/request/:id', adminController.requestDetail )
router.get('/produtos/:id', adminController.produtoDetail )
router.get('/produtos/:id/delete', adminController.produtoDelete )
router.post('/produtos/:id/edit', adminController.produtoEdit )







module.exports = router;