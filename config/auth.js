const LocalStrategy = require("passport-local").Strategy
const mongoose = require('mongoose')
var bcrypt = require('bcryptjs');
const cookieParser = require('cookie-parser');
// Model de usuário 
require('../models/users')
const User = mongoose.model('Users')

module.exports = function(passport){
   
    
    passport.use( new LocalStrategy({usernameField: 'email', passwordField: "password"}, (email, password, done) => {
        
        User.findOne({email: email}).then((user) => {
           
            if(!user){
                
                return done(null, false, {massage: 'Essa conta não existe'})
            }
            
            
            
            var userPass = user.password
            bcrypt.compare(password, userPass, (err, res) => {
                
                if(res){
                    console.log('aqui')
                    
                    return done(null, user)
                }else{
                    console.log(err)
                    return done(null, false, {message:'Senha incorreta'})
                }
            })
        })

    }))
    passport.serializeUser((user, done) => {
        
        done(null, user.id)
    })
    passport.deserializeUser((id, done) =>{
        User.findById(id, (err, user) => {
           
            done(err, user)
        })
    })
}