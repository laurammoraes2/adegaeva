const mongoose = require('mongoose');
const Requests = mongoose.model('Requests');
const Products = mongoose.model('Products');
const Users = mongoose.model('Users');
const Blog = mongoose.model('Blogs');

const bcrypt = require('bcryptjs');
const passport = require('passport');
const util = require("util");
const multer = require("multer");


var fs = require('fs');
var path = require('path');

const storage = multer.diskStorage({
  destination: function(request, file, callback){
    callback(null, './public/uploads'); 
  },
  filename: function(request, file, callback){
    callback(null,Date.now() + file.originalname)
  },
});

const upload = multer({
  storage:storage, 
  limits:{
    fieldSize: 1024*1024*3,

  }
});

// Página inicial

exports.home = async (req, res) => {
  try {
    const data = await Products.find({});
    const blog = await Blog.find({}).limit(3)
 
   
    res.render('pages/customer/index', {data: data, blog: blog})
    
  } catch (error) {
    
  }
 
}

// Dashboard
exports.dashboard = async (req, res) => {
    res.render('pages/admin/dashboard', {page:'sidebar'})
}

// Produtos CRUD
// Visualizar produtos
exports.products = async (req, res) => {
  try {
    const data = await Products.find({});
    
    res.render('pages/admin/admin-products', {page:'sidebar', data: data})
    
  } catch (e) {
    res.status(500).send({message: 'Falha ao carregar as menções.'});
  }
  
}
// Get da página de criação dos produtos
exports.createproducts = async (req, res) => {
  res.render('pages/admin/admin-createproducts')
}

// Post da página de criação dos produtos - realiza a criação do produto
// exports.formproducts = upload.single('filename'), async (req, res) => {
//   try {
//     console.log(req.body)
//   console.log('oi')
    
//   } catch (error) {
//     console.log(error)
//   }
  
 
// }
      
  
 
  



       
         

      
// Update dos produtos 
exports.updateProduct = function (req, res) {
  Products.findByIdAndUpdate(req.params.id, {$set: req.body}, function (err, product) {
      if (err) return next(err);
      console.log('User udpated.');
      res.redirect('/produtos')
  });
};
// Delete dos produtos
exports.deleteProduct = function (req, res) {
  

  Products.findByIdAndRemove(req.params.id, function (err) {
      if (err) return next(err);
      console.log('Deleted successfully!');
      
      res.redirect('/produtos')
  })
};




 


   

// Pedidos
// Mostra os pedidos realizados
exports.requests = async (req, res) => {
  try {
    const data = await Requests.find({});
    
    
    
    res.render('pages/admin/admin-requests', {page:'sidebar', data: data})
    
  } catch (e) {
    res.status(500).send({message: 'Falha ao carregar as menções.'});
  }
  
};
exports.pedidosFinalizados = async (req, res) => {
  try {
    const data = await Requests.find({status: 'Finalizado'});
    
    
    
    res.render('pages/admin/admin-finalizados', {page:'sidebar', data: data})
    
  } catch (e) {
    res.status(500).send({message: 'Falha ao carregar as menções.'});
  }
  
};

exports.requestDetail = async(req, res) => {
  const id= req.params.id
  try{
    const data = await Requests.findOne({_id: id })
    const pedido = JSON.parse(data.cesta)
    res.render('pages/admin/requestDetail', {data: data, pedido: pedido})
    
  }catch(e){
    res.status(500).send({message: 'Falha ao carregar as menções.'});
  }

};
exports.produtoDetail = async (req, res) => {
  try { 
  const data = await Products.findOne({_id: req.params.id});
  
  res.render('pages/admin/admin-productDetail', {data: data})
    
  } catch (error) {
    // console.log(error)
  }

  
}
  

// Update dos pedidos com envio de email
exports.updateRequest = function (req, res) {
  Requests.findByIdAndUpdate(req.params.id, {$set: req.body}, function (err, product) {
      if (err) return next(err);
      res.send('User udpated.');
  });
};

// Cancelamento de pedido com envio de email
exports.deleteRequest = function (req, res) {
  

  Requests.findByIdAndRemove(req.params.id, function (err) {
      if (err) return next(err);
      res.send('Deleted successfully!');
  })
};
exports.produtoDelete = function (req, res) {
  

  Products.findByIdAndRemove(req.params.id, function (err) {
      if (err) return next(err);
      console.log('Deleted successfully!');
      
      res.redirect('/produtos')
  })
};
exports.produtoEdit = async (req, res) => {
  console.log(req.body)
  const name = req.body.name;
  const description = req.body.description;
  const price = req.body.price;
  const quantity = req.body.quantity;
  const type = req.body.tipo;
  
  var newvalues = { $set: {name: name, description: description, price: price, quantity: quantity, type: type} };
  Products.updateOne({_id: req.params.id }, newvalues, function(err) {
    if (err) throw err;
    console.log("1 document updated");
    res.redirect('/produtos')
    
  });
  
 
 
    
}


// Clientes 
// Mostra os clientes cadastrados
exports.customers = async (req, res) => {
  try {
    const data = await Users.find({eAdmin: "1"});
  
    res.render('pages/admin/admin-customers', {page:'sidebar', data: data})
    
  } catch (e) {
    res.status(500).send({message: 'Falha ao carregar as menções.'});
  }
  
}
exports.customersDetail = async (req, res) => {
  try {
    const id = req.params.id;
    const data = await Users.findOne({_id: id
    });
  
    res.render('pages/admin/admin-detail', { data: data})
    
  } catch (e) {
    res.status(500).send({message: 'Falha ao carregar as menções.'});
  }
  
}
exports.editadmin = async (req, res) => {
  console.log(req.body)
  const name = req.body.name;
  const email = req.body.email;
  var senha = req.body.senha;
  bcrypt.genSalt(10, (erro, salt) => {
    bcrypt.hash(senha, salt, (erro, hash) => {
      console.log(senha)
      if(erro){
       
        res.redirect('/')
      }
      senha = hash
      console.log(senha)
      var newvalues = { $set: {name: name, email: email, password: senha} };
    Users.updateOne({_id: req.params.id }, newvalues, function(err) {
    if (err) throw err;
    console.log("1 document updated");
    res.redirect('/admin')
    
  });

    })
  
  })
 
 
    
}

exports.createAdmin = async (req, res) => {
  try {
    const data = await Users.find({});
    
    res.render('pages/admin/admin-create', {page:'sidebar', data: data})
    
  } catch (e) {
    res.status(500).send({message: 'Falha ao carregar as menções.'});
  }
  
}


// Criação de Admin
exports.create = async (req, res) => {
  const name = req.body.name;
  
  const email = req.body.email;
  var password = req.body.password;
  const password2 = req.body.password2;
  const telphone = req.body.telphone;
  const eAdmin = 1;
  var erros = [];

  if(!name || typeof name == undefined || name == null ){
    erros.push({ texto: "Nome inválido"})
  }
  if(!email || typeof email == undefined || name == null){
    erros.push({texto: "Email inválido" })
  }
  if(!password || typeof password == undefined || name == null){
    erros.push({texto: "Senha inválida" })
  }
  if(req.body.password.length < 5 ){
    erros.push({texto: "Senha muito curta" })
  }
  if(req.body.password != req.body.password2){
    erros.push({texto: "As senhas são diferentes, tente novamente" })
  }
  if(!telphone || typeof telphone == undefined || telphone == null){
    erros.push({texto: "Telefone inválido" })
  }

  if(erros.length >0 ){
      res.render('pages/error')
      
  }else{
    Users.findOne({email: req.body.email}).then((user) =>{  
     
      
        
          const admin = new Users({
            name,
            email,
            password,
            telphone,
            eAdmin
           
          }); 
          bcrypt.genSalt(10, (erro, salt) => {
            bcrypt.hash(admin.password, salt, (erro, hash) => {
              if(erro){
               
                res.redirect('/')
              }
              admin.password = hash
              admin.save().then(() => {
                
                res.redirect('/admin')
              }).catch((err) => {
                
              })
              });

            })
          
      
          
      

    })
  }
    }

// Update de admin
exports.updateAdmin = function (req, res) {
  Users.findByIdAndUpdate(req.params.id, {$set: req.body}, function (err, product) {
      if (err) return next(err);
      res.send('User udpated.');
  });
};

// Delete de admin
exports.deleteAdmin = function (req, res) {
  

  Users.findByIdAndRemove(req.params.id, function (err) {
      if (err) return next(err);
    
      res.redirect('/admin')
  })
};


// Blog
exports.blog = async (req, res) => {
  try {
    const data = await Blog.find({});
    
    res.render('pages/admin/admin-blog', {page:'sidebar', data: data})
    
  } catch (e) {
    res.status(500).send({message: 'Falha ao carregar as menções.'});
  }
  

}

exports.blogdetail = async (req, res) => {
  try {
    const id= req.params.id
    const data = await Blog.findOne({_id: id});
    
    res.render('pages/admin/blogdetail', {page:'sidebar', data: data})
    
  } catch (e) {
    res.status(500).send({message: 'Falha ao carregar as menções.'});
  }
  
}

exports.createblog = async (req, res) => {
  
    res.render('pages/admin/create-blog')
 
}
// Novidades 

exports.novidades = async (req, res) => {
  try {
    const data = await Blog.find({});
    
    res.render('pages/customer/novidades', { data: data})
    
  } catch (e) {
    res.status(500).send({message: 'Falha ao carregar as menções.'});
  }
  
}
exports.novidadeArtigo = async (req, res) => {
  try {
    const id= req.params.id
    const data = await Blog.findOne({_id: id});
    
    res.render('pages/customer/artigo', { data: data})
    
  } catch (e) {
    res.status(500).send({message: 'Falha ao carregar as menções.'});
  }
  
}

// Update de admin
exports.updateBlog = function (req, res) {
  console.log(req.body)
  const title = req.body.titulo;
  const subtitle = req.body.subtitulo;
  const text = req.body.texto;
  var newvalues = { $set: {title: title, subtitle: subtitulo, text: text} };
  Blog.updateOne({_id: req.params.id }, newvalues, function(err) {
  if (err) throw err;
  console.log("1 document updated");
  res.redirect('/adminblog')
  
});
  
};

// Delete de admin
exports.deleteBlog = function (req, res) {
  
  
  Blog.findOneAndDelete(req.params.id, function (err) {
      if (err) return next(err);
    
      res.redirect('/blog')
  })
};

