const mongoose = require('mongoose');
const Products = mongoose.model('Products');
const Users = mongoose.model('Users');
const Requests = mongoose.model('Requests');
const mercadopago = require ('mercadopago');

const { json } = require('body-parser');
mercadopago.configure({
  access_token: 'APP_USR-8089417633582493-042900-b88dd22fe83bff1a9a81871762a71bde-463038908'
});



exports.dashboard = async (req, res) => {
  res.render('pages/admin/dashboard', {page:'sidebar'})
}

exports.addItem = async (req, res) => {
  console.log(req.body.quantidade)
  
  var carrinho = req.session.cart; 
  var id_product = req.params.id || req.body.produto_id; 

  console.log(carrinho)
  
    var pos = -1;
    for (let i = 0; i < carrinho.length; i++) {
      if(carrinho[i].id == id_product){
        
        pos = i;
        break;

      }
     
    
  }
  const data = await Products.findOne({_id: req.params.id});
  if(pos == -1){
    var price = req.body.quantidade * data.price;
   
    var produto ={
      id: data._id,
      name: data.name,
      price: data.price,
      quantidade: JSON.parse(req.body.quantidade), 
      priceTotal: price , 
      quantity: data.quantity
    }
    

 
    req.session.cart.push(produto)


  }else{
    var produto = carrinho[pos]; 
    produto.quantidade = produto.quantidade +  JSON.parse(req.body.quantidade); 
    produto.priceTotal = produto.quantidade*produto.price;
    carrinho[pos] = produto;
    
    i = carrinho.length

  }
  var Total =  0
  
  for (let i = 0; i < carrinho.length; i++) {
    var Total =  Total + carrinho[i].priceTotal
    
  }
   
  
  
    
  console.log(carrinho)
  

  res.render('pages/customer/cesta', {produto: carrinho, lastPrice: Total})

  
} 
exports.listItems = async (req, res) => {
  var carrinho = req.session.cart;

 console.log(carrinho)
  var Total =  0
  
  // for (let i = 0; i < carrinho.length; i++) {
  //   var Total =  Total + carrinho[i].priceTotal
    
  // }
  res.render('pages/customer/cesta', {produto: carrinho, lastPrice: Total})

  
}

exports.atualizar = async (req, res) => {
  var carrinho = req.session.cart;
  console.log(req.body)

 
  // var Total =  0
  
  // for (let i = 0; i < carrinho.length; i++) {
  //   var Total =  Total + carrinho[i].priceTotal
    
  // }
  // res.render('pages/cesta', {produto: carrinho, lastPrice: Total})

  
}
exports.checkout = async (req, res) => {
  var carrinho = req.session.cart;
  var passport = req.session.passport
  
  
  if(passport == undefined){
    res.redirect('/login')
  }
  const data = await Users.find({_id: passport.user});
  console.log(carrinho)
  
  var Total =  0
  
  for (let i = 0; i < carrinho.length; i++) {
    var Total =  Total + carrinho[i].priceTotal
    
  }
  res.render('pages/customer/checkout', {produto: carrinho, lastPrice: Total, data: data})

  
}

exports.finish = async (req, res) => {
  
  const name_customer = req.body.customerName +" " + req.body.customerSobrenome;
  const email_customer = req.body.customerEmail;
  const tel_customer = req.body.customerTelphone;
  const cep_customer = req.body.customerCep;
  const address_customer = req.body.customerEndereco + " " + req.body.customerBairro + " " + req.body.customerEstado + " " + req.body.customerCidade;
  const number_customer = req.body.customerNumero;
  const cesta = JSON.stringify(req.session.cart);
  const status = "Novo";
 
  var carrinho = req.session.cart;
  var Total =  0
  
  for (let i = 0; i < carrinho.length; i++) {
    var Total =  Total + carrinho[i].priceTotal
    
  }
  carrinho.map(carro => {
    
    let preference = {
      items: [
        {
          title: 'AdegaEva',
          unit_price: Total ,
          quantity: carro.quantidade,
        }
      ]
    };
    mercadopago.preferences.create(preference).then(function(response){
  // Este valor substituirá a string "<%= global.id %>" no seu HTML
    global.id = response.body.id;
   

    const request = new Requests({
      name_customer,
      email_customer,
      tel_customer,
      cep_customer,
      address_customer,
      number_customer,
      cesta,
      status,


    })
    request.save().then(() => {
              
      res.render('pages/customer/pagamento', {reference: global.id, data: request})
    }).catch((err) => {
      console.log(err)
    });
   
    
  }).catch(function(error){
    console.log(error);
  });

  
    
    
  })


  
  
}

// Atualização do status do pedido 
// Confirmar pagamento
exports.confirmarPagamento = async (req, res) => {
  const id = req.params.id;

  const data = await Requests.findOne({_id: id})
  const status = "Pagamento confirmado";
  
  var newvalues = { $set: {status: status } };
  Requests.updateOne({_id: req.params.id }, newvalues, function(err) {
    if (err) throw err;
    
    
  });
  
 
  // var Total =  0
  
  // for (let i = 0; i < carrinho.length; i++) {
  //   var Total =  Total + carrinho[i].priceTotal
    
  // }
  // res.render('pages/cesta', {produto: carrinho, lastPrice: Total})

  
}
// Falar com o cliente 
exports.falarCliente = async (req, res) => {
  const id = req.params.id;

  const data = await Requests.findOne({_id: id})
  const status = "Falar com Cliente";
  
  var newvalues = { $set: {status: status } };
  Requests.updateOne({_id: req.params.id }, newvalues, function(err) {
    if (err) throw err;
    
    
  });
  
}
// Em separação
exports.emSeparacao = async (req, res) => {
  const id = req.params.id;

  const data = await Requests.findOne({_id: id})
  const status = "Em separação";
  
  var newvalues = { $set: {status: status } };
  Requests.updateOne({_id: req.params.id }, newvalues, function(err) {
    if (err) throw err;
    
    
  });
  
}
// Pedido enviado
exports.enviado = async (req, res) => {
  const id = req.params.id;

  const data = await Requests.findOne({_id: id})
  const status = "Enviado";
  
  var newvalues = { $set: {status: status } };
  Requests.updateOne({_id: req.params.id }, newvalues, function(err) {
    if (err) throw err;
    
    
  });
  
}
// Pedido finalizado
exports.finalizar = async (req, res) => {
  const id = req.params.id;

  const data = await Requests.findOne({_id: id})
  const status = "Finalizado";
  
  var newvalues = { $set: {status: status } };
  Requests.updateOne({_id: req.params.id }, newvalues, function(err) {
    if (err) throw err;
    
    
  });
  
}