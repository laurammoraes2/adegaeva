
const mongoose = require('mongoose');
const { product } = require('./admin');

const Products = mongoose.model('Products');
const Cestas = mongoose.model('Cestas');
const Users = mongoose.model('Users');



// Listagem de produtos
exports.shop = async (req, res) => {
    try {
      if(req.session.cart == undefined){
        req.session.cart = []
        
      }
        const data = await Products.find({});
        
        const id = data.map(id => 
          id.filename)
          
       
       
        res.render('pages/customer/shop', { data: data})
        
      } catch (e) {
        res.status(500).send({message: 'Falha ao carregar os produtos.'});
      }

}
exports.shopFilter = async (req, res) => {
  try {
   
    if(req.session.cart == undefined){
      req.session.cart = []
      
    }
      const data = await Products.find({type: req.params.type});
    
     
      res.render('pages/customer/shop', { data: data})
      
     
     
      
      
    } catch (e) {
      res.status(500).send({message: 'Falha ao carregar os produtos.'});
    }

}


exports.cart = async (req, res) => {
   
      const user = JSON.stringify(req.session.passport.user)
      const data = await Cestas.find({id_user:user});
    
      const produto = data.map(sku => sku.produto)
      
    
    
      const sku = await Products.find({sku: produto});
     
      res.render('pages/customer/cesta', {produto: sku})
      // res.render('pages/cesta')

      
   
      
}
exports.checkout = async (req, res) => {

      const data = await Cestas.find({id_user: req.session.passport.user});
      
      const produto = data.map(sku => sku.produto)
     
    
      const sku = await Products.find({sku: produto});
    

  res.render('pages/customer/checkout', {produto:sku})
}

// Descrição do produto
exports.descriptionProd = async (req, res) => {
  try { 
  const data = await Products.findOne({_id: req.params.id});
  
  
  res.render('pages/customer/product', {data: data})
    
  } catch (error) { 
    // console.log(error)
  }

  
  
  
}

exports.pagamento = async(req,res) => {

  // const nomeCompleto = req.body.name; 
  // const email = req.body.email;
  // const cep =req.body.cep;
  // const rua = req.body.rua;
  // const bairro = req.body.bairro;
  // const numero = req.body.numero;
  // const estado = req.body.estado;
  // const cidade = req.body.cidade;
  


  // let preference = {
  //   items: [
  //     {
  //       title: 'Meu produto',
  //       unit_price: 100,
  //       quantity: 1,
  //     }
  //   ]
  // };
  
  // mercadopago.preferences.create(preference)
  // .then(function(response){
  // // Este valor substituirá a string "<%= global.id %>" no seu HTML
  //   global.id = response.body.id;
  // }).catch(function(error){
  //   console.log(error);
  // });
}


exports.cesta = async (req, res) => {
  
  try{
    const data = await Products.findOne({sku: req.params.sku});
    const id_user = req.session.passport.user;
    const produto = data.sku
  
          const cesta = new Cestas({
           produto,
           id_user
          });
         
          cesta.save().then(() => {
                
             
            }).catch((err) => {
           
            })

  }catch(e){
    console.log(e)
  }
  
       
}



