const mongoose = require('mongoose');
const Users = mongoose.model('Users');
const bcrypt = require('bcryptjs');
const passport = require('passport')


// Usuários 
// Listar usuários cadastrados

exports.listUsers = async (req, res) => {
  try {
    const data = await Users.find({});
    res.status(200).send(data);
    
  } catch (e) {
    res.status(500).send({message: 'Falha ao carregar as menções.'});
  }
};

// Login
exports.listUser = async (req, res, next) => {  
  passport.authenticate('local',{
    
    successRedirect: "/",
    failureRedirect:'/login',
    failureFlash: true,
    
  })(req, res, next)
 
  
 
};

// Criar usuário comum
exports.createUser = async (req, res) => {
  const name = req.body.name;
  
  const email = req.body.email;
  var password = req.body.password;
  const password2 = req.body.password2;
  const telphone = req.body.telphone;
  const eAdmin = 0;
  var erros = [];

  if(!name || typeof name == undefined || name == null ){
    erros.push({ texto: "Nome inválido"})
  }
  if(!email || typeof email == undefined || name == null){
    erros.push({texto: "Email inválido" })
  }
  if(!password || typeof password == undefined || name == null){
    erros.push({texto: "Senha inválida" })
  }
  if(req.body.password.length < 5 ){
    erros.push({texto: "Senha muito curta" })
  }
  if(req.body.password != req.body.password2){
    erros.push({texto: "As senhas são diferentes, tente novamente" })
  }
  if(!telphone || typeof telphone == undefined || telphone == null){
    erros.push({texto: "Telefone inválido" })
  }

  if(erros.length >0 ){
      res.render('pages/error')
      
  }else{
    Users.findOne({email: req.body.email}).then((user) =>{
      if(user){
        
        res.redirect('/login')
      }else{
        
          const user = new Users({
            name,
            email,
            password,
            telphone,
            eAdmin
           
          });

        
          bcrypt.genSalt(10, (erro, salt) => {
            bcrypt.hash(user.password, salt, (erro, hash) => {
              if(erro){
               
                res.redirect('/')
              }
              user.password = hash
              user.save().then(() => {
                
                res.redirect('/login')
              }).catch((err) => {
                
              })
              });

            })
          
      
          
      
          
       
         
        }
      

      
    }).catch((err) => {
      req.flash("error_msg", "Houve um erro interno")
      res.redirect('/')
    })
  }
}
  
  

 
  
  
 
// Delete usuário

  exports.deleteUser = function (req, res) {
  

    Users.findByIdAndRemove(req.params.id, function (err) {
        if (err) return next(err);
        res.send('Deleted successfully!');
    })
};

//Update usuário

  exports.updateUser = function (req, res) {
    Users.findByIdAndUpdate(req.params.id, {$set: req.body}, function (err, product) {
        if (err) return next(err);
        res.send('User udpated.');
    });
};


